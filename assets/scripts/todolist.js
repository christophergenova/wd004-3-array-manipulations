//Our task.
//Create a todo list with the following functions.
//
//1. showToDos
// will return an array of all todos
//
//2. addTodos(todo)
// will add a new todo in our array of todos
//
//3. updateTodo()
// given an index, will update the todo
///
//4. deleteTodo()
// given an index, will delete a todo

// Steps
// 1. create an empty todos array

const todos = [];

// 2. create a function that will add a new todo.

function addTodo(task) {
	// things to consider:
	//1. what is the data type of the todo data? string? object?
	//2. what are the parameters that this function needs?

	const newTodo = task;

	//push the newTodo into our todos array.
	todos.push(newTodo);
	return "Succesfully added" + newTodo;


}


function showTodos(){
	return todos;

}

function updateTodo(index, updatedTask){
	// Things to consider.
	//1. WHen updating a data, we need something to identify which data we want to update. In this case, we will use the index.
	//2. We need the updated value.

	todos[index]= updatedTask;
	return "Succesfully updated task!";

}

function deleteTodo(index){
	//Things to consider:
	//1. when deleting a datam we need something to identify which data we want to delete.

	todos.splice(index, 1);
	return "Succesfully deleted task!";

}

//THIS IS HOW YOU CREATE TO DO LIST