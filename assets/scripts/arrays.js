//array -- is a colection of related data. Denoted by [].
//ex. 

const fruits = ['apple', 'banana', 'kiwi'];

const ages = [1,2,3,4];

const students = [
	{name: "brandon", age: 11 },
	{name: "brenda", age: 12 },
	{name: "celmer", age: 15 },
	{name: "archie", age: 16 }
];
// gusto malaman kung paano na aacess ung object 

//it is recommended to use plural form when naming an array, and singular when naming object

//to access an array, we need to use bracket notation and the index of the item we want to access.
//
//index is the position of the data relative to the position of the first data.
//
//to add a data in an array, we will use the method .push();
//
//array.push(dataWeWantToAdd);

//the method will return the lenght of the array.

//lenght means # of the data inside an array

//The new data will be added at the end.

//----------------------

// in output: .push is for adding a data into array object

//to update fruits[1] = "NewData";

//update students[2] = {Name: "cacay", age: 19};

//students[1].age = 15; -- How to UPDATE age of specific Index or Data array
//output is: will have a new data from previous index/location

//to add or update previous index: 
//students[2] = {Name: "cacay", age: 19};
//array[index] = newValue;

//to delete a data at the end of an array, we use the method pop();

//fruits.pop()
//array.pop();
//it will return the deleted data;
//.pop is always deleting the last

//to delete a data at the start of an array, we use the method shift();
//array.shift();
//it will return the deleted data;

//to add a data in the beginning of array, we use the method unshift(dataWeWantToAdd);
//array.unshift(dataWeWantToAdd);
//this will return the length of the array

//push and unshift = adding
//pop and shift = deleting

//push--- add/ end
//pop --- delete/ end
//shift ---delete / start
//unshift ---add / start
//
//.splice();
//it takes at least 2 agruments.
//.splice( startingIndex, noOfItemsYouWantToDelete --optional dataWeWantToInsert );

//example: fruits;
//fruits.splice(1, 1);
//this will return an array of deleted items;

//fruits.splice(1, 0, "orange");
//return no return, but it will add new
//fruits.splice(1, 0, "bayabas", "duhat");
//return blank array
//but output added duhat and bayabas

//fruits.splice(1, 1, "orange");
//papalitan nya ung nasa index[1] ng Orange

//if the deleteCount is 0, it will just insert the new data from the starting index






